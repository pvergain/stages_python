#!/usr/bin/env python3

import pendulum

# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
project = "Stages Python 3e"
author = "id3 Technologies"
now = pendulum.now("Europe/Paris")
today = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H{now.minute:02}"
copyright = f"2016-{now.year}, id3 Technologies"
version = today
# The full version, including alpha/beta/rc tags.
release = version
language = "fr"
exclude_patterns = ["_build", ".venv", ".git", "_static", "_themes", "src", "images"]
pygments_style = "sphinx"
todo_include_todos = True
html_theme = "cloud"
html_theme_path = ["_themes"]
html_static_path = ["_static"]
intersphinx_mapping = {
    "django": ("https://gdevops.gitlab.io/tuto_django/", None),
    "documentation": ("https://gdevops.gitlab.io/tuto_documentation/", None),
    "project": ("https://gdevops.gitlab.io/tuto_project/", None),
}
