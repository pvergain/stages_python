
.. index::
   pair: Installation ; Python
   pair: Documentation ; GIMP


.. _installation_python:

=====================================================================================
Installation de l'environnement de programmation Python 3.5, Anaconda, Gimp, Sphinx
=====================================================================================


.. toctree::
   :maxdepth: 2

   2018/2018
   2016/2016
   git/git
   gimp/gimp





