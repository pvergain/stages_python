

.. index::
   pair: stage ; Hugo


.. _reprise_2018_12_17_reprise:

=================================================
Reprise du projet en 2018 pour le stage d'Hugo
=================================================

.. contents::
   :depth: 3


Description
============

Reprise du projet pour le stage de 3e d'Hugo.


Mise du projet sous framagit
==============================

.. seealso::

   - https://framagit.org/pvergain/stages_python


.. figure:: projet_framagit.png
   :align: center


Ce qui a changé depuis 2016
=============================

- utilisation de gitlab
- utilisation de python 3.7 au lieu de python 3.5
- utilisation de pipenv au lieu pip
- utilisation de visualstudio code au lieu de pycharm
- développement sous GNU/Linux au lieu de Windows


Proposition
=============



