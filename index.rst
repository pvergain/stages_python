

.. index::
   pair: Stage; 3e (2016-2017)


.. _stage_python:

========================================================================
Stages Python 3e
========================================================================

.. seealso::

   - https://pvergain.frama.io/stages_python/
   - https://framagit.org/pvergain/stages_python/boards
   - https://framagit.org/pvergain/stages_python
   - https://www.python.org/
   - https://twitter.com/gvanrossum
   - https://docs.python.org/3/
   - https://twitter.com/realpython
   - https://twitter.com/PythonWeekly

.. sidebar:: Python scientifique

    :Projet: Stages Python pour 3e
    :Release: |release|
    :Date: |today|
    :Authors: Patrick

    - :ref:`genindex`
    - :ref:`search`

    - https://docs.python.org/3/
    - https://www.continuum.io
    - https://twitter.com/SciPyTip


.. figure::  installation/2016/python35/Python_logo_and_wordmark.svg.png
   :align: center

Introduction
=============

- stage effectué par :ref:`Ethan le 15 février 2019 <action_2019>`

- stage effectué par :ref:`Hugo les 16 et 17 décembre 2018 <action_2018>`

- Stage effectué par :ref:`Anaelle et Sofia les 13 et 14 octobre 2016 <action_2016>`
  pour une initiation au langage Python.


Programmes Python
=================

.. toctree::
   :maxdepth: 6

   programmes/programmes

Actions
========

.. toctree::
   :maxdepth: 6

   actions/actions


Environnements de développement Python
=======================================

.. toctree::
   :maxdepth: 5

   dev/dev


Installation de Python et autres outils
=======================================

.. toctree::
   :maxdepth: 5

   installation/installation


Glossaire
=========

.. toctree::
   :maxdepth: 5

   glossaire/glossaire





